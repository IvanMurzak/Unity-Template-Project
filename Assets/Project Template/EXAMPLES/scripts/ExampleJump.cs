﻿using Project.Template;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class ExampleJump : ActionKeyDown
{
    public float force = 400;

    new Rigidbody rigidbody;

    protected override void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
        base.Awake();
    }

    protected override void ExecuteAction()
    {
        base.ExecuteAction();
        rigidbody.AddForce(Vector3.up * force);
    }
}
