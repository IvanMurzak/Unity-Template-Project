﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

namespace Project.Template.Editor
{
    //[CustomPropertyDrawer(typeof(ProjectEvent.ProjectEventUnit), true)]
    //public class ProjectEventUnitDrawer : PropertyDrawer
    //{
    //    ComponentField component = new ComponentField();
    //    FunctionField function = new FunctionField();
    //    InputField input = new InputField();

    //    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    //    {
    //        SerializedProperty inputValues = property.FindPropertyRelative(InputField.INPUT_VALUES);
    //        float one = base.GetPropertyHeight(property, label);
    //        return one * 2 + 10;
    //    }

    //    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    //    {
    //        // Using BeginProperty / EndProperty on the parent property means that
    //        // prefab override logic works on the entire property.
    //        EditorGUI.BeginProperty(position, label, property);

    //        // Don't make child fields be indented
    //        int indent = EditorGUI.indentLevel;
    //        EditorGUI.indentLevel = 0;

    //        // -----------------------------------------------------------------------------------------
    //        // -----------------------------------------------------------------------------------------

    //        Rect componentRect = new Rect(position); componentRect.height = base.GetPropertyHeight(property, label);
    //        Rect functionRect = new Rect(position); functionRect.height = base.GetPropertyHeight(property, label);
    //        Rect inputRect = new Rect(functionRect); inputRect.y += inputRect.height;

    //        component.OnGUI(componentRect, property);
    //        function.OnGUI(functionRect, property);
    //        input.OnGUI(inputRect, property);

    //        // -----------------------------------------------------------------------------------------
    //        // -----------------------------------------------------------------------------------------

    //        // Set indent back to what it was
    //        EditorGUI.indentLevel = indent;

    //        EditorGUI.EndProperty();
    //    }
        

    //    private class ComponentField
    //    {
    //        public const string INSTANCE = "instance";
    //        public const string EXECUTE_MODE = "executeMod";
    //        public const int WIDTH = 140;

    //        public void OnGUI(Rect position, SerializedProperty property)
    //        {
    //            Rect rectComponent = new Rect(position.x, position.y + position.height, WIDTH, position.height); 
    //            Rect rectMode = new Rect(position.x, position.y, WIDTH - 18, position.height);

    //            EditorGUI.PropertyField(rectComponent, property.FindPropertyRelative(INSTANCE), GUIContent.none);
    //            EditorGUI.PropertyField(rectMode, property.FindPropertyRelative(EXECUTE_MODE), GUIContent.none);
    //        }
    //    }

    //    private class FunctionField
    //    {
    //        public const string SELECTED_MEMBER = "selectedMember";
    //        public const string SELECTED_MEMBER_INDEX = "selectedMemberIndex";

    //        string[] Options(SerializedProperty property)
    //        {
    //            List<string> options = new List<string>();
    //            options.Add("No Function");

    //            SerializedProperty instance = property.FindPropertyRelative(ComponentField.INSTANCE);
    //            if (instance != null)
    //            {
    //                if (instance.objectReferenceValue != null && instance.objectReferenceValue as Component)
    //                {
    //                    SerializedProperty inputValues = property.FindPropertyRelative(InputField.INPUT_VALUES);
    //                    SerializedProperty selectedMember = property.FindPropertyRelative(SELECTED_MEMBER);
    //                    SerializedProperty selectedMemberIndex = property.FindPropertyRelative(SELECTED_MEMBER_INDEX);

    //                    int index = 1;
    //                    Component component = instance.objectReferenceValue as Component;
    //                    Component[] components = component.gameObject.GetComponents(typeof(Component));
    //                    Array.ForEach(components, c =>
    //                    {
    //                        Array.ForEach(c.GetType().GetProperties(), p =>
    //                        {
    //                            if (p.CanWrite && p.CanRead && !p.IsSpecialName)
    //                            {
    //                                if (selectedMemberIndex.intValue == index)
    //                                {
    //                                    // selectedMember. = p;
    //                                }
    //                                options.Add(string.Format("{0}/{2}\t{1}", c.GetType().Name, p.PropertyType.Name, p.Name));
    //                                index++;
    //                            }
    //                        });

    //                        Array.ForEach(c.GetType().GetMethods(), m =>
    //                        {
    //                            if (m.IsPublic && !m.IsConstructor && !m.IsAbstract && !m.IsSpecialName)
    //                            {
    //                                ParameterInfo[] parametersInfo = m.GetParameters();
    //                                if (selectedMemberIndex.intValue == index)
    //                                {
    //                                    inputValues.ClearArray();
    //                                    int arrayIndex = 0;
    //                                    Array.ForEach(parametersInfo, mp =>
    //                                    {
    //                                        inputValues.InsertArrayElementAtIndex(arrayIndex);
    //                                        if (mp.ParameterType == typeof(int))    inputValues.GetArrayElementAtIndex(arrayIndex).intValue = 0;
    //                                        if (mp.ParameterType == typeof(float))  inputValues.GetArrayElementAtIndex(arrayIndex).floatValue = 0;
    //                                        if (mp.ParameterType == typeof(double)) inputValues.GetArrayElementAtIndex(arrayIndex).doubleValue = 0;
    //                                        if (mp.ParameterType == typeof(bool))   inputValues.GetArrayElementAtIndex(arrayIndex).boolValue = false;
    //                                        if (mp.ParameterType == typeof(string)) inputValues.GetArrayElementAtIndex(arrayIndex).stringValue = "";
    //                                        if (mp.ParameterType == typeof(Color))  inputValues.GetArrayElementAtIndex(arrayIndex).colorValue = Color.black;
    //                                        if (mp.ParameterType == typeof(Vector2)) inputValues.GetArrayElementAtIndex(arrayIndex).vector2Value = Vector2.zero;
    //                                        if (mp.ParameterType == typeof(Vector3)) inputValues.GetArrayElementAtIndex(arrayIndex).vector3Value = Vector3.zero;
    //                                        if (mp.ParameterType == typeof(Vector4)) inputValues.GetArrayElementAtIndex(arrayIndex).vector4Value = Vector4.zero;
    //                                        if (mp.ParameterType == typeof(UnityEngine.Object)) inputValues.GetArrayElementAtIndex(arrayIndex).objectReferenceValue = null;

    //                                        arrayIndex++;
    //                                    });
    //                                }
    //                                StringBuilder parameters = new StringBuilder();
    //                                Array.ForEach(parametersInfo, mp =>
    //                                {
    //                                    parameters = parameters.Append(mp.ParameterType.Name)
    //                                        .Append(", ");
    //                                });
    //                                if (parameters.Length > 0)
    //                                    parameters = parameters.Remove(parameters.Length - 2, 2);

    //                                options.Add(string.Format("{0}/{1} ({2})", c.GetType().Name, m.Name, parameters.ToString()));
    //                                index++;
    //                            }
    //                        });
    //                    });
    //                }
    //            }
    //            return options.ToArray();
    //        }

    //        public void OnGUI(Rect position, SerializedProperty property)
    //        {
    //            float x = position.x + ComponentField.WIDTH + 5;
    //            Rect rect = new Rect(x, position.y, position.width - x + 14, position.height);

    //            SerializedProperty selectedMemberIndex = property.FindPropertyRelative(SELECTED_MEMBER_INDEX);

    //            selectedMemberIndex.intValue = Mathf.Min(selectedMemberIndex.intValue, Options(property).Length);
    //            selectedMemberIndex.intValue = EditorGUI.Popup(rect, selectedMemberIndex.intValue, Options(property));
    //        }
    //    }

    //    private class InputField
    //    {
    //        public const string INPUT_VALUES = "inputValues";

    //        public void OnGUI(Rect position, SerializedProperty property)
    //        {
    //            float x = position.x + ComponentField.WIDTH + 5;
    //            Rect rect = new Rect(x, position.y, position.width - x + 14, position.height);

    //            SerializedProperty inputValues = property.FindPropertyRelative(INPUT_VALUES);
    //            SerializedProperty selectedMemberIndex = property.FindPropertyRelative(FunctionField.SELECTED_MEMBER_INDEX);
    //            if (selectedMemberIndex.intValue > 0)
    //            {
    //                for(int i = 0; i < inputValues.arraySize; i++)
    //                {
    //                    Rect unitRect = new Rect(rect);
    //                    unitRect.x += rect.width / inputValues.arraySize * i;
    //                    unitRect.width = rect.width / inputValues.arraySize;

    //                    // EditorGUI.Vector2Field
    //                    EditorGUI.PropertyField(unitRect, inputValues.GetArrayElementAtIndex(i), GUIContent.none);
    //                }
    //            }
    //            else
    //            {
    //                inputValues.ClearArray();
    //            }
    //        }
    //    }
    //}
}