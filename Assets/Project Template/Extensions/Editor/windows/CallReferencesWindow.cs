﻿using UnityEditor;
using UnityEngine;

namespace Project.Template.Internal
{
    public class CallReferencesWindow : EditorWindow
    {
        [MenuItem("HotKey/Special Command %f")]
        [MenuItem("Window/Call References")]
        public static void ShowWindow()
        {
            EditorWindow.GetWindow<CallReferencesWindow>("Call References");
        }

        enum State { SELECTED_MULTIPLE, SELECTED_ZERO, SCANNING, DONE }
        State state = State.SELECTED_ZERO;

        void FindReferences(ProjectEvent action)
        {
            #region Validate
            if (Selection.gameObjects.Length == 0)
            {
                state = State.SELECTED_ZERO;
                return;
            }
            if (Selection.gameObjects.Length > 1)
            {
                state = State.SELECTED_MULTIPLE;
                return;
            }
            #endregion

            
        }

        void OnGUI()
        {

        }
    }
}
