﻿using UnityEditor;
using UnityEngine;

namespace Project.Template.Internal
{
    public class ProjectTemplateSettingsWindow : EditorWindow
    {
        [MenuItem("Window/Project Template")]
        public static void ShowWindow()
        {
            EditorWindow.GetWindow<ProjectTemplateSettingsWindow>("Project Template Settings");
        }
        
        void OnGUI()
        {
            SerializedObject so = new SerializedObject(ProjectTemplateSettings.Instance);
            foreach(System.Reflection.FieldInfo field in ProjectTemplateSettings.Instance.GetType().GetFields())
            {
                if (!field.IsNotSerialized && !field.IsStatic && field.IsPublic)
                {
                    EditorGUILayout.PropertyField(so.FindProperty(field.Name), true);
                }
            }
            so.ApplyModifiedProperties();
            AssetDatabase.SaveAssets();

            if (GUILayout.Button("Refresh Scenes"))
            {
                SceneEnumGenerator.Generate();
            }
        }
    }    
}
