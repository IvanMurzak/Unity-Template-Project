﻿using UnityEditor;
using UnityEngine;
using System.IO;
using System.Collections.Generic;

namespace Project.Template.Internal
{
    public class SceneEnumGenerator : UnityEditor.AssetPostprocessor
    {
        private const string SCENES_ENUM_NAME = "BuildingScenes";
        private static string[] TRIGGER_FILE_ENDS = new string[] { ".unity", "EditorBuildSettings.asset" };

        private static string GetPath()
        {
            return Project.Template.Internal.Path.SCRIPTS + "utils/enum/" + SCENES_ENUM_NAME + ".cs";
        }

        private static string ConvertToEnumName(string name)
        {
            return name
                .Replace(" ", "_")
                .Replace("@", "_")
                .Replace(":", "_")
                .Replace(";", "_")
                .Replace("=", "_")
                .Replace("-", "_")
                .Replace("!", "_")
                .Replace("#", "_")
                .Replace("$", "_")
                .Replace("%", "_")
                .Replace("^", "_")
                .Replace("*", "_")
                .Replace("(", "_")
                .Replace(")", "_")
                .Replace(".", "_")
                .Replace(",", "_")
                .Replace("~", "_");
        }

        private static string[] SceneNames()
        {
            List<string> temp = new List<string>();
            foreach (UnityEditor.EditorBuildSettingsScene scene in UnityEditor.EditorBuildSettings.scenes)
            {
                if (scene.enabled)
                {
                    string name = scene.path.Substring(scene.path.LastIndexOf('/') + 1);
                    name = name.Substring(0, name.Length - 6);
                    temp.Add(name);
                }
            }
            return temp.ToArray();
        }

        private static bool IsTriggers(string str)
        {
            return IsTriggers(str, TRIGGER_FILE_ENDS);
        }

        private static bool IsTriggers(string str, string[] ends)
        {
            foreach (string end in ends)
                if (str.EndsWith(end)) return true;
            return false;
        }
        
        static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
        {
            if (!ProjectTemplateSettings.Instance.AutoRefreshingScenes)
                return;

            foreach (string path in importedAssets)
            {
                if (IsTriggers(path))
                {
                    Generate();
                    return;
                }
            }
            foreach (string path in deletedAssets)
            {
                if (IsTriggers(path))
                {
                    Generate();
                    return;
                }
            }

            foreach (string path in movedAssets)
            {
                if (IsTriggers(path))
                {
                    Generate();
                    return;
                }
            }
        }

        public static void Generate()
        {
            string path = GetPath();
            if (ProjectTemplateSettings.Instance.IsDebug)
                Debug.Log("Generating Scene's enum: " + path);
            using (StreamWriter outfile = new StreamWriter(path))
            {
                outfile.WriteLine("using System.ComponentModel;");
                outfile.WriteLine("");
                outfile.WriteLine("// Auto generated code");
                outfile.WriteLine("");
                outfile.WriteLine("");
                outfile.WriteLine("namespace Project.Template");
                outfile.WriteLine("{");
                outfile.WriteLine("\t[System.Serializable]");
                outfile.WriteLine("\tpublic enum " + SCENES_ENUM_NAME);
                outfile.WriteLine("\t{");

                string[] sceneNames = SceneNames();
                foreach (string sceneName in sceneNames)
                {
                    outfile.WriteLine("\t\t[Description(\"" + sceneName + "\")]");
                    outfile.WriteLine("\t\t" + ConvertToEnumName(sceneName) + " = " + sceneName.GetHashCode() + ",");
                    outfile.WriteLine("");
                }

                outfile.WriteLine("\t}");
                outfile.WriteLine("}");
            }
            AssetDatabase.Refresh();
        }
    }
}
