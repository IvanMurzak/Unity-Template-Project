﻿using UnityEngine;
using System.Collections;
using Project.Template;

public class PositionAttacher : MonoBehaviour
{
    public Transform target;
	public Vector3 globalOffset;

    [System.Serializable]
    public struct Freeze
    {
        public bool x;
        public bool y;
        public bool z;
    }
    public Freeze freezePosition = new Freeze();
    public bool freezeRotation = true;

    public bool physix = false;

    private void Awake()
    {
        if (target == null) gameObject.LogError("target is empty! Please select a target or delete " + typeof(PositionAttacher).Name);
    }

    void LateUpdate ()
    {
        if (!physix) UpdatePosition();
    }

    void FixedUpdate()
    {
        if (physix) UpdatePosition();
    }

    void UpdatePosition()
    {
        if (target == null) return;
        if (freezePosition.x || freezePosition.y || freezePosition.z)
        {
            Vector3 position = new Vector3(
                freezePosition.x ? target.position.x + globalOffset.x : transform.position.x,
                freezePosition.y ? target.position.y + globalOffset.y : transform.position.y,
                freezePosition.z ? target.position.z + globalOffset.z : transform.position.z);
            transform.position = position;
        }
        if (freezeRotation) transform.rotation = target.rotation;
    }
}
