﻿using UnityEngine;

namespace Project.Template
{
    [RequireComponent(typeof(AudioSource))]
    public class AudioPitchRandomizer : MonoBehaviour
    {
        private AudioSource[] audios;

        [Range(0f, 1f)]
        public float randomPitchOffset = 0.5f;

        private float[] originalPitch;

        private void OnValidate()
        {
            randomPitchOffset = Mathf.Max(0f, Mathf.Min(1f, randomPitchOffset));
        }

        protected virtual void Start()
        {
            audios = GetComponents<AudioSource>();
            originalPitch = new float[audios.Length];
            for (int index = 0; index < audios.Length; index++)
            {
                originalPitch[index] = audios[index].pitch;
                audios[index].pitch = CalcRandomPitch(index);
            }
        }

        public void Play()
        {
            int index = Random.Range(0, audios.Length);
            audios[index].pitch = CalcRandomPitch(index);
            if (audios[index].enabled) audios[index].Play();
        }

        private float CalcRandomPitch(int index)
        {
            return originalPitch[index] + originalPitch[index] * Random.Range(randomPitchOffset / -2f, randomPitchOffset / 2f);
        }

        public int Count()
        {
            return audios.Length;
        }
    }
}
