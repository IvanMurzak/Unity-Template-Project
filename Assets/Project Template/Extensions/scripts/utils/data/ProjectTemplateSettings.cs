﻿using UnityEngine;

namespace Project.Template.Internal
{
    [System.Serializable]
    public class ProjectTemplateSettings : ScriptableObject
    {
        public const string FILE_NAME = "ProjectTemplateSettings";


        public DebugData debug = new DebugData();

        [Space]
        public bool AutoRefreshingScenes = true;
        public BuildingScenes loadingScene = new BuildingScenes();

        [System.Serializable]
        public class DebugData
        {
            public bool editorLog = true;
            public bool buildLog = true;
        }

        public bool IsDebug
        {
            get
            {
#if UNITY_EDITOR
                return debug.editorLog;
#else
                return debug.buildLog;
#endif
            }
        }

        private static ProjectTemplateSettings Load()
        {
            ProjectTemplateSettings result = Resources.Load<ProjectTemplateSettings>(FILE_NAME);
            if (result == null)
            {
                result = new ProjectTemplateSettings();
#if UNITY_EDITOR
                UnityEditor.AssetDatabase.CreateAsset(result, Path.SETTINGS);
#endif
            }
            return result;
        }

        private static ProjectTemplateSettings instance;
        public static ProjectTemplateSettings Instance
        {
            get
            {
                if (instance == null)
                    instance = Load();
                return instance;
            }
        }
    }
}
