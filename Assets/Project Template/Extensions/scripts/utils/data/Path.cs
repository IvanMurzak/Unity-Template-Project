﻿namespace Project.Template.Internal
{
    public static class Path
    {
        // folders
        public const string PROJECT_TEMPLATE_ROOT = "Assets/Project Template/";
        public const string EXTENSIONS = "Extensions/";
        public const string RESOURCES = PROJECT_TEMPLATE_ROOT + "Resources/";
        public const string SCRIPTS = PROJECT_TEMPLATE_ROOT + EXTENSIONS + "scripts/";


        // files
        public const string SETTINGS = RESOURCES + ProjectTemplateSettings.FILE_NAME + ".asset";
    }
}
