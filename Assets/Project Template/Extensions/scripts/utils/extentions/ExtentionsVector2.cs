﻿using UnityEngine;

namespace Project.Template
{
    /// <summary>
    /// Extension methods for UnityEngine.Vector2.
    /// </summary>
    public static class ExtentionsVector2
    {
        /// <summary>
        /// Sets X value to the Vector2.
        /// </summary>
        /// <param name="v">Destination Vector2.</param>
        /// <param name="x">X value.</param>
        public static Vector2 SetX(this Vector2 v, float x)
        {
            v.x = x;
            return v;
        }

        /// <summary>
        /// Sets Y value to the Vector2.
        /// </summary>
        /// <param name="v">Destination Vector2.</param>
        /// <param name="y">Y value.</param>
        public static Vector2 SetY(this Vector2 v, float y)
        {
            v.y = y;
            return v;
        }
    }
}
