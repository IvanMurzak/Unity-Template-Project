﻿using UnityEngine;
using Project.Template.Internal;
using System;

namespace Project.Template
{
    public static class ExtentionsGameObject
    {
        public static string SceneName()
        {
#if UNITY_5_2 || UNITY_5_1 || UNITY_5_0
            return Application.loadedLevelName;
#else
            return UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;
#endif
        }

        private static string StringPattern(GameObject gameObject, string message)
        {
            return "[" + SceneName() + "]:" + gameObject.name + ":" + message;
        }

        public static void LogError(this GameObject gameObject, string message)
        {
            if (ProjectTemplateSettings.Instance.IsDebug)
                Debug.LogErrorFormat(gameObject, StringPattern(gameObject, message));
        }

        public static void Log(this GameObject gameObject, string message)
        {
            if (ProjectTemplateSettings.Instance.IsDebug)
                Debug.LogFormat(gameObject, StringPattern(gameObject, message));
        }

        public static T AddComponent<T>(this GameObject gameObject, Action<T> action) where T : Component
        {
            using (gameObject.Deactivate())
            {
                T component = gameObject.AddComponent<T>();
                if (action != null) action(component);
                return component;
            }
        }

        public static IDisposable Deactivate(this GameObject gameObject)
        {
            return new GameObjectDeactivateSection(gameObject);
        }

        private class GameObjectDeactivateSection : IDisposable
        {
            GameObject go;
            bool oldState;
            public GameObjectDeactivateSection(GameObject gameObject)
            {
                go = gameObject;
                oldState = go.activeSelf;
                go.SetActive(false);
            }
            public void Dispose()
            {
                go.SetActive(oldState);
            }
        }
    }
}