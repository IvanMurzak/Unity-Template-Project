﻿using UnityEngine;

namespace Project.Template
{
    /// <summary>
    /// Extension methods for UnityEngine.Vector3.
    /// </summary>
    public static class ExtentionsVector3
    {
        /// <summary>
        /// Sets X value to the Vector3.
        /// </summary>
        /// <param name="v">Destination Vector3.</param>
        /// <param name="x">X value.</param>
        public static Vector3 SetX(this Vector3 v, float x)
        {
            v.x = x;
            return v;
        }

        /// <summary>
        /// Sets Y value to the Vector3.
        /// </summary>
        /// <param name="v">Destination Vector3.</param>
        /// <param name="y">Y value.</param>
        public static Vector3 SetY(this Vector3 v, float y)
        {
            v.y = y;
            return v;
        }

        /// <summary>
        /// Sets Z value to the Vector3.
        /// </summary>
        /// <param name="v">Destination Vector3.</param>
        /// <param name="z">Z value.</param>
        public static Vector3 SetZ(this Vector3 v, float z)
        {
            v.z = z;
            return v;
        }
    }
}
