using System.ComponentModel;

// Auto generated code


namespace Project.Template
{
	[System.Serializable]
	public enum BuildingScenes
	{
		[Description("TriggerExample")]
		TriggerExample = -2110799694,

		[Description("SceneLoadingExample")]
		SceneLoadingExample = 968225818,

		[Description("AsyncLoadingScene")]
		AsyncLoadingScene = -275289812,

	}
}
