﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System;
using Project.Template.Internal;
using UniRx;

namespace Project.Template
{
    public class TimeUtils : SingletonComponent<TimeUtils>
    {
        public FloatReactiveProperty timeScale = new FloatReactiveProperty(1);

        private void Awake()
        {
            timeScale.Subscribe(x => OnTimeScaleUpdated(x)).AddTo(this); // OnTimeScaleUpdated(timeScale.Value);
        }

        private void OnTimeScaleUpdated(float timeScale)
        {
            Time.timeScale = timeScale;
        }
    }
}
