﻿using UnityEngine;

namespace Project.Template
{
    public class SingletonComponent<T> : MonoBehaviour where T : Component
    {
        private static T instance;

        public static T Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<T>();
                    if (instance == null)
                    {
                        GameObject obj = new GameObject();
                        obj.name = typeof(T).Name;
                        obj.hideFlags = HideFlags.DontSave;
                        instance = obj.AddComponent<T>();
                    }
                }

                return instance;
            }
        }

        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
            if (instance == null)
                instance = this as T;
        }
    }
}
