﻿namespace Project.Template
{
    public interface IEditorValidator
    {
#if UNITY_EDITOR
        string OnValidateInEditor();
#endif
    }
}