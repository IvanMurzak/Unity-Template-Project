using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Project.Template
{
    public static class ValidateUtils
    {
        public static bool Validate(GameObject gameObject, params UnityEvent[] actions)
        {
            bool result = true;
            if (Application.isEditor)
            {
                foreach (UnityEvent action in actions)
                {
                    for (int index = 0; index < action.GetPersistentEventCount(); index++)
                    {
                        UnityEngine.Object temp = action.GetPersistentTarget(index);
                        if (temp == null)
                        {
                            gameObject.LogError("TARGET[" + index + "] is null");
                            result = false;
                        }
                        if (action.GetPersistentMethodName(index) == null)
                        {
                            gameObject.LogError("METHOD[" + index + "] is null");
                            result = false;
                        }
                    }
                }
            }
            return result;
        }

        public static bool Validate<T0>(GameObject gameObject, params UnityEvent<T0>[] actions)
        {
            bool result = true;
            if (Application.isEditor)
            {
                foreach (UnityEvent<T0> action in actions)
                {
                    for (int index = 0; index < action.GetPersistentEventCount(); index++)
                    {
                        UnityEngine.Object temp = action.GetPersistentTarget(index);
                        if (temp == null)
                        {
                            gameObject.LogError("TARGET[" + index + "] is null");
                            result = false;
                        }
                        if (action.GetPersistentMethodName(index) == null)
                        {
                            gameObject.LogError("METHOD[" + index + "] is null");
                            result = false;
                        }
                    }
                }
            }
            return result;
        }

        public static bool Validate<T0, T1>(GameObject gameObject, params UnityEvent<T0, T1>[] actions)
        {
            bool result = true;
            if (Application.isEditor)
            {
                foreach (UnityEvent<T0, T1> action in actions)
                {
                    for (int index = 0; index < action.GetPersistentEventCount(); index++)
                    {
                        UnityEngine.Object temp = action.GetPersistentTarget(index);
                        if (temp == null)
                        {
                            gameObject.LogError("TARGET[" + index + "] is null");
                            result = false;
                        }
                        if (action.GetPersistentMethodName(index) == null)
                        {
                            gameObject.LogError("METHOD[" + index + "] is null");
                            result = false;
                        }
                    }
                }
            }
            return result;
        }

        public static bool Validate<T0, T1, T2>(GameObject gameObject, params UnityEvent<T0, T1, T2>[] actions)
        {
            bool result = true;
            if (Application.isEditor)
            {
                foreach (UnityEvent<T0, T1, T2> action in actions)
                {
                    for (int index = 0; index < action.GetPersistentEventCount(); index++)
                    {
                        UnityEngine.Object temp = action.GetPersistentTarget(index);
                        if (temp == null)
                        {
                            gameObject.LogError("TARGET[" + index + "] is null");
                            result = false;
                        }
                        if (action.GetPersistentMethodName(index) == null)
                        {
                            gameObject.LogError("METHOD[" + index + "] is null");
                            result = false;
                        }
                    }
                }
            }
            return result;
        }

        public static bool Validate<T0, T1, T2, T3>(GameObject gameObject, params UnityEvent<T0, T1, T2, T3>[] actions)
        {
            bool result = true;
            if (Application.isEditor)
            {
                foreach (UnityEvent<T0, T1, T2, T3> action in actions)
                {
                    for (int index = 0; index < action.GetPersistentEventCount(); index++)
                    {
                        UnityEngine.Object temp = action.GetPersistentTarget(index);
                        if (temp == null)
                        {
                            gameObject.LogError("TARGET[" + index + "] is null");
                            result = false;
                        }
                        if (action.GetPersistentMethodName(index) == null)
                        {
                            gameObject.LogError("METHOD[" + index + "] is null");
                            result = false;
                        }
                    }
                }
            }
            return result;
        }
    }
}