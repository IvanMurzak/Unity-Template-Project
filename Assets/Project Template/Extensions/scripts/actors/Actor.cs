﻿using UnityEngine;
using Sirenix.OdinInspector;

namespace Project.Template
{
    public class Actor : SerializedMonoBehaviour
    {
        protected virtual void Awake()
        {
#if UNITY_EDITOR
            if (this is IEditorValidator)
            {
                string error = ((IEditorValidator)this).OnValidateInEditor();
                if (error != null) gameObject.LogError(error);
            }
#endif
        }
    }
}
