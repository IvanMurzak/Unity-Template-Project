﻿namespace Project.Template
{
    public class ActorAdvancedButton : ActorUI, IEditorValidator
    {
        public AdvancedAnalyticsButton button;

        public string OnValidateInEditor()
        {
            bool valid = button.transform.FindChildByName("content") != null;
            if (!valid) return "'content' doesn't exist in the right hierarchy";
            return null;
        }

        protected override void Awake()
        {
            base.Awake();
            button.gameObject.name = gameObject.name;
        }
    }
}
