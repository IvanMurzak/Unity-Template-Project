﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

namespace Project.Template
{
    public class AnalyticsButton : Button
    {
        public bool sendAnalytics = true;

        protected override void Start()
        {
            base.Start();
            ValidateUtils.Validate(gameObject, onClick);
        }

        public override void OnPointerClick(PointerEventData eventData)
        {
            base.OnPointerClick(eventData);
            if (sendAnalytics) UltimateAnalytics.AnalyticsLogEvent(gameObject, UltimateAnalytics.Action.button, null, 0);
        }

        public void SimulateClick()
        {
            if (onClick != null) onClick.Invoke();
        }
    }
}
