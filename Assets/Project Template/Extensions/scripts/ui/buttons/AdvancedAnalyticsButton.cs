﻿using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Project.Template
{
    public class AdvancedAnalyticsButton : Selectable
    {
        public bool sendAnalyticsOnDown = true;
        public bool sendAnalyticsOnUp = true;
        public bool sendAnalyticsOnEnter = true;
        public bool sendAnalyticsOnExit = true;
        
        public ProjectEvent<PointerEventData> 
            onDownEvent = new ProjectEvent<PointerEventData>(),
            onUpEvent = new ProjectEvent<PointerEventData>(),
            onEnterEvent = new ProjectEvent<PointerEventData>(),
            onExitEvent = new ProjectEvent<PointerEventData>();

        public ProjectEvent
            onDown = new ProjectEvent(),
            onUp = new ProjectEvent(),
            onEnter = new ProjectEvent(),
            onExit = new ProjectEvent();

        protected override void Start()
        {
            base.Start();
            ValidateUtils.Validate(gameObject, onDown, onUp, onEnter, onExit);
            ValidateUtils.Validate(gameObject, onDownEvent, onUpEvent, onEnterEvent, onExitEvent);
        }

        public override void OnPointerDown(PointerEventData eventData)
        {
            base.OnPointerDown(eventData);
            if (onDown != null) onDown.Invoke();
            if (onDownEvent != null) onDownEvent.Invoke(eventData);
            if (sendAnalyticsOnDown) UltimateAnalytics.AnalyticsLogEvent(gameObject, UltimateAnalytics.Action.buttonOnDown, null, 0);
        }

        public override void OnPointerUp(PointerEventData eventData)
        {
            base.OnPointerUp(eventData);
            if (onUp != null) onUp.Invoke();
            if (onUpEvent != null) onUpEvent.Invoke(eventData);
            if (sendAnalyticsOnUp) UltimateAnalytics.AnalyticsLogEvent(gameObject, UltimateAnalytics.Action.buttonOnUp, null, 0);
        }
        
        public override void OnPointerEnter(PointerEventData eventData)
        {
            base.OnPointerEnter(eventData);
            if (onEnter != null) onEnter.Invoke();
            if (onEnterEvent != null) onEnterEvent.Invoke(eventData);
            if (sendAnalyticsOnEnter) UltimateAnalytics.AnalyticsLogEvent(gameObject, UltimateAnalytics.Action.buttonOnEnter, null, 0);
        }

        public override void OnPointerExit(PointerEventData eventData)
        {
            base.OnPointerExit(eventData);
            if (onExit != null) onExit.Invoke();
            if (onExitEvent != null) onExitEvent.Invoke(eventData);
            if (sendAnalyticsOnExit) UltimateAnalytics.AnalyticsLogEvent(gameObject, UltimateAnalytics.Action.buttonOnExit, null, 0);
        }
    }
}