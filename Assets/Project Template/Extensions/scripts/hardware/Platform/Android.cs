﻿using UnityEngine;

namespace Project.Template.Extensions.scripts.hardware.platform
{
#if UNITY_ANDROID
    static class Android
    {
        public static string ID
        {
            get
            {
                AndroidJavaClass up = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                AndroidJavaObject currentActivity = up.GetStatic<AndroidJavaObject>("currentActivity");
                AndroidJavaObject contentResolver = currentActivity.Call<AndroidJavaObject>("getContentResolver");
                AndroidJavaClass secure = new AndroidJavaClass("android.provider.Settings$Secure");
                string android_id = secure.CallStatic<string>("getString", contentResolver, "android_id");
                return android_id;
            }
        }
    }
#endif
}
