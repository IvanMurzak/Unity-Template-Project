﻿using UnityEngine;

namespace Project.Template.Extensions.scripts.hardware.platform
{
#if UNITY_IOS
    static class IOS
    {
        public static string ID
        {
            get
            {
                return SystemInfo.deviceUniqueIdentifier;
            }
        }
    }
#endif
}
