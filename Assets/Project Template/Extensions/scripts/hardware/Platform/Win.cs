﻿using UnityEngine;

namespace Project.Template.Extensions.scripts.hardware.platform
{
#if UNITY_STANDALONE_WIN
    static class Win
    {
        public static string ID
        {
            get
            {
                return SystemInfo.deviceUniqueIdentifier;
            }
        }
    }
#endif
}
