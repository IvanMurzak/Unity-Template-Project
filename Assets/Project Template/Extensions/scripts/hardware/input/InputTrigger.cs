﻿using UniRx;
using UniRx.Triggers;
using UnityEngine;

namespace Project.Template.Extensions.scripts.hardware.input.extension
{
    class InputTrigger : ObservableTriggerBase
    {
        public string button;
        public KeyListener.State state;

        [HideInInspector]
        public Subject<Unit> onAction = new Subject<Unit>();

        private void Update()
        {
            if (state == KeyListener.State.DOWN)
                if (Input.GetButtonDown(button))
                    onAction.OnNext(Unit.Default);

            if (state == KeyListener.State.UP)
                if (Input.GetButtonUp(button))
                    onAction.OnNext(Unit.Default);

            if (state == KeyListener.State.CONTINUES_PRESSING)
                if (Input.GetButton(button))
                    onAction.OnNext(Unit.Default);
        }

        protected override void RaiseOnCompletedOnDestroy()
        {
            if (onAction != null) 
                onAction.OnCompleted();
        }
    }
}
