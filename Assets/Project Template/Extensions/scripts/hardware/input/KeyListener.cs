﻿using UnityEngine;
using UniRx;
using System.Collections.Generic;
using Project.Template.Extensions.scripts.hardware.input.extension;

namespace Project.Template
{
    class KeyListener : SingletonComponent<KeyListener>
    {
        public enum State { DOWN, UP, CONTINUES_PRESSING }

        private Dictionary<UniRx.Tuple<string, State>, InputTrigger> observableButtons = 
            new Dictionary<UniRx.Tuple<string, State>, InputTrigger>();

        #region Overrided methods
        public System.IDisposable SubscribeOnDown(string key, GameObject gameObject, UniRx.IObserver<UniRx.Unit> observer)
        {
            return SubscribeOn(new UniRx.Tuple<string, State>(key, State.DOWN), gameObject, observer);
        }

        public System.IDisposable SubscribeOnUp(string key, GameObject gameObject, UniRx.IObserver<UniRx.Unit> observer)
        {
            return SubscribeOn(new UniRx.Tuple<string, State>(key, State.UP), gameObject, observer);
        }

        public System.IDisposable SubscribeOnContinuesPressing(string key, GameObject gameObject, UniRx.IObserver<UniRx.Unit> observer)
        {
            return SubscribeOn(new UniRx.Tuple<string, State>(key, State.CONTINUES_PRESSING), gameObject, observer);
        }

        public System.IDisposable SubscribeOn(string key, State state, GameObject gameObject, UniRx.IObserver<UniRx.Unit> observer)
        {
            return SubscribeOn(new UniRx.Tuple<string, State>(key, state), gameObject, observer);
        }
        #endregion

        public System.IDisposable SubscribeOn(UniRx.Tuple<string, State> keyState, GameObject gameObject, UniRx.IObserver<UniRx.Unit> observer)
        {
            if (!observableButtons.ContainsKey(keyState))
            {
                InputTrigger trigger = gameObject.AddComponent<InputTrigger>();
                trigger.button = keyState.Item1;
                trigger.state = keyState.Item2;
                observableButtons.Add(keyState, trigger);
            }

            return observableButtons[keyState].onAction
                .Subscribe(observer)
                .AddTo(gameObject);
        }
    }
}
