﻿using Project.Template.Extensions.scripts.hardware.platform;

public static class Device
{
    public static string Get
    {
        get
        {
#if UNITY_ANDROID
            return Android.ID;
#elif UNITY_IOS
            return IOS.ID;
#elif UNITY_STANDALONE_WIN
            return Win.ID;
#else
            return null;
#endif
        }
    }
}
