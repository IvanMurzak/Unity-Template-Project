﻿using UnityEngine;
using Project.Template.Internal;

namespace Project.Template
{
    public class SceneLoader : ActionInvoker
    {
        public BuildingScenes scene;
        public bool loadAsync = false;  
        public bool showLoading = false;

        protected override void ExecuteAction()
        {
            base.ExecuteAction();

            string sceneName;
            if (showLoading)
            {
                BuildingScenes loadingScene = ProjectTemplateSettings.Instance.loadingScene;
                sceneName = loadingScene.GetDescription();

                gameObject.Log("Creating middle gameObject");
                GameObject middleGameObject = new GameObject("LoadingMiddleObject");
                gameObject.Log(string.Format("Adding {0} component", typeof(LoadingScene).Name));
                middleGameObject.AddComponent<LoadingScene>().scene = scene;
            }
            else sceneName = scene.GetDescription();

            gameObject.Log("Loading scene:" + sceneName);

#if UNITY_5_2 || UNITY_5_1 || UNITY_5_0
            if (loadAsync) Application.LoadLevelAsync(sceneName);
            else Application.LoadLevel(sceneName);
#else
            if (loadAsync) UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(sceneName);
            else UnityEngine.SceneManagement.SceneManager.LoadScene(sceneName);
#endif
        }

        private class LoadingScene : MonoBehaviour
        {
            public BuildingScenes scene;

            private void Start()
            {
                string sceneName = scene.GetDescription();
                gameObject.Log("Loading scene:" + sceneName);
                UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(sceneName);
            }
        }
    }
}
