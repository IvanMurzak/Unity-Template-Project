﻿using UnityEngine;

namespace Project.Template
{
    public abstract class ActionTriggerBase : ActionInvoker
    {
        public LayerMask mask = -1;

        protected virtual void OnDetect(GameObject other)
        {
            if (mask.CheckLayermask(other)) Invoke();
        }
    }
}
