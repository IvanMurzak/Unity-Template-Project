﻿using UnityEngine;

namespace Project.Template
{
    [RequireComponent(typeof(Collider))]
    public class ActionTriggerEnter : ActionTrigger
    {
        protected virtual void OnCollisionEnter(Collision collision)
        {
            OnDetect(collision.gameObject);
        }

        protected virtual void OnTriggerEnter(Collider other)
        {
            OnDetect(other.gameObject);
        }
    }
}
