﻿using UnityEngine;

namespace Project.Template
{
    [RequireComponent(typeof(Collider2D))]
    public class ActionTriggerExit2D : ActionTrigger2D
    {
        protected virtual void OnCollisionExit2D(Collision2D collision)
        {
            OnDetect(collision.gameObject);
        }

        protected virtual void OnTriggerExit2D(Collider2D other)
        {
            OnDetect(other.gameObject);
        }
    }
}
