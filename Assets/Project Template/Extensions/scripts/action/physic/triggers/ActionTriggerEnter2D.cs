﻿using UnityEngine;

namespace Project.Template
{
    [RequireComponent(typeof(Collider2D))]
    public class ActionTriggerEnter2D : ActionTrigger2D
    {
        protected virtual void OnCollisionEnter2D(Collision2D collision)
        {
            OnDetect(collision.gameObject);
        }

        protected virtual void OnTriggerEnter2D(Collider2D other)
        {
            OnDetect(other.gameObject);
        }
    }
}
