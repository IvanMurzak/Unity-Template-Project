﻿using UnityEngine;

namespace Project.Template
{
    [RequireComponent(typeof(Collider))]
    public class ActionTriggerExit : ActionTrigger
    {
        protected virtual void OnCollisionExit(Collision collision)
        {
            OnDetect(collision.gameObject);
        }

        protected virtual void OnTriggerExit(Collider other)
        {
            OnDetect(other.gameObject);
        }
    }
}
