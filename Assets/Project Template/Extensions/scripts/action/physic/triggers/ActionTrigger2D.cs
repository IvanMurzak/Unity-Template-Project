﻿using UnityEngine;

namespace Project.Template
{
    public abstract class ActionTrigger2D : ActionTriggerBase
    {
#if UNITY_EDITOR
        protected override void Awake()
        {
            base.Awake();

            if (mask != -1)
            {
                for (int layer = 0; layer < 32; layer++)
                {
                    bool collideWithMask = mask.CheckLayermask(layer);
                    bool collideWithPhysic = !Physics2D.GetIgnoreLayerCollision(layer, gameObject.layer);
                    if (collideWithMask && !collideWithPhysic)
                        gameObject.LogError("Impossible Collision detected in mask. Layer:" + LayerMask.LayerToName(layer));
                }
            }
        }
#endif
    }
}
