﻿using UnityEngine;

namespace Project.Template
{ 
    public class BreakJointListener2D : BreakJointListenerBase
    {
        void OnJointBreak2D(Joint2D joint)
        {
            onBreak(joint.breakForce);
        }
    }
}
