﻿using UnityEngine;

namespace Project.Template
{
    public abstract class BreakJointListenerBase : MonoBehaviour
    {
        public delegate void OnBreak(float breakForce);
        public OnBreak onBreak;
    }
}
