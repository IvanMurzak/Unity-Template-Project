﻿namespace Project.Template
{ 
    public class BreakJointListener : BreakJointListenerBase
    {
        void OnJointBreak(float breakForce)
        {
            onBreak(breakForce);
        }
    }
}
