﻿using UnityEngine;

namespace Project.Template
{
    public abstract class ActionBreakJointGeneric<T, K> : ActionInvoker 
        where T : Component 
        where K : BreakJointListenerBase
    {
        public bool listenChildren = true;

        protected override void Awake()
        {
            RecursiveAddListeners(transform);
            base.Awake();
        }

        protected virtual void RecursiveAddListeners(Transform transform)
        {
            AddListener(transform.GetComponent<T>());

            if (listenChildren)
                foreach (Transform child in transform)
                    RecursiveAddListeners(child);
        }

        protected virtual void AddListener(T joint)
        {
            if (joint != null)
            {
                K listener = joint.gameObject.AddComponent<K>();
                listener.onBreak += (brokenJointx) => Invoke();
            }
        }
    }
}
