﻿using UniRx;

namespace Project.Template
{
    public class ActionKeyUp : ActionInvoker
    {
        public bool sendAnalytics = true;
        public string key;

        protected override void Start()
        {
            base.Start();
            KeyListener.Instance.SubscribeOnUp(key, gameObject,
                Observer.Create<Unit>(_ =>
                {
                    if (isActiveAndEnabled)
                    {
                        Invoke();
                        if (sendAnalytics)
                        {
                            UltimateAnalytics.AnalyticsLogEvent(gameObject, UltimateAnalytics.Action.key, key, 0);
                        }
                    }
                }));
        }
    }
}