﻿using UniRx;

namespace Project.Template
{
    public class ActionKey : ActionInvoker
    {
        public bool sendAnalytics = true;
        public string key;

        protected override void Start()
        {
            base.Start();
            KeyListener.Instance.SubscribeOnContinuesPressing(key, gameObject,
                Observer.Create<Unit>(_ =>
                {
                    if (isActiveAndEnabled)
                    {
                        Invoke();
                        if (sendAnalytics)
                        {
                            UltimateAnalytics.AnalyticsLogEvent(gameObject, UltimateAnalytics.Action.key, key, 0);
                        }
                    }
                }));
        }
    }
}