﻿using UnityEngine;

namespace Project.Template
{
    [RequireComponent(typeof(AudioSource))]
    public class ActionAudio : MonoBehaviour
    {
        public ProjectEvent onStart, onEnd;

        private AudioSource audioSource;
        private bool isStarted = false;

        protected virtual void Awake()
        {
            audioSource = GetComponent<AudioSource>();
            ValidateUtils.Validate(gameObject, onStart);
            ValidateUtils.Validate(gameObject, onEnd);
        }

        void Update()
        {
            if (audioSource.isPlaying && !isStarted) OnStart();
            if (!audioSource.isPlaying && isStarted) OnEnd();

            isStarted = audioSource.isPlaying;
        }

        void OnEnd()
        {
            if (onEnd != null) onEnd.Invoke();
        }

        void OnStart()
        {
            if (onStart != null) onStart.Invoke();
        }
    }
}
