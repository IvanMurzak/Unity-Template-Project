﻿using UnityEngine;

namespace Project.Template
{
    [RequireComponent(typeof(AudioPitchRandomizer))]
    public abstract class ActionAudioTriggerGeneric<T> : ActionInvoker where T : ActionTriggerBase
    {
        public float cooldown = 0.3f;

        private float lastPlayTime = 0;
        private T actionTrigger;
        private AudioPitchRandomizer audioPitchRandomizer;

        protected override void Awake()
        {
            audioPitchRandomizer = GetComponent<AudioPitchRandomizer>();
            actionTrigger = GetComponent<T>();
            actionTrigger.action.AddListener(() => Invoke());
            base.Awake();
        }

        protected override void ExecuteAction()
        {
            base.ExecuteAction();
            if (Time.time > lastPlayTime + cooldown)
            {
                lastPlayTime = Time.time;
                audioPitchRandomizer.Play();
            }
        }
    }
}
