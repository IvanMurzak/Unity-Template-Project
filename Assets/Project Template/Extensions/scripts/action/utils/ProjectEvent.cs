﻿namespace Project.Template
{
    [System.Serializable]
    public class ProjectEvent : UnityEngine.Events.UnityEvent
    {
    }

    [System.Serializable]
    public class ProjectEvent<T0> : UnityEngine.Events.UnityEvent<T0>
    {
    }

    [System.Serializable]
    public class ProjectEvent<T0, T1> : UnityEngine.Events.UnityEvent<T0, T1>
    {
    }

    [System.Serializable]
    public class ProjectEvent<T0, T1, T2> : UnityEngine.Events.UnityEvent<T0, T1, T2>
    {
    }
    [System.Serializable]
    public class ProjectEvent<T0, T1, T2, T3> : UnityEngine.Events.UnityEvent<T0, T1, T2, T3>
    {
    }
}
