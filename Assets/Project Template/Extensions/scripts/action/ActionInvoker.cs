﻿using UnityEngine;
using System;
using UniRx;

namespace Project.Template
{
    public class ActionInvoker : MonoBehaviour
    {
        public ProjectEvent action = new ProjectEvent();
        public Properties actionProperties = new Properties();

        [HideInInspector]
        public float invokeDelay { set { actionProperties.invokeDelay = value; } }
        [HideInInspector]
        public bool invokeOnce { set { actionProperties.invokeOnce = value; } }
        [HideInInspector]
        public bool ignoreTimeScale { set { actionProperties.ignoreTimeScale = value; } }
        [HideInInspector]
        public bool invokeNOW
        {
            set { actionProperties.invokeNOW.SetValueAndForceNotify(value); }
            private get { return actionProperties.invokeNOW.Value; }
        }

        protected virtual void Awake()
        {
            ValidateUtils.Validate(gameObject, action);

            actionProperties.invokeNOW
                .DistinctUntilChanged()
                .Subscribe(x => { if (x) Invoke(); })
                .AddTo(this);
        }

        protected virtual void Start()
        {
            //
        }

        public void Invoke()
        {
            if (actionProperties.Invoke())
            {
                if (actionProperties.invokeDelay > 0)
                {
                    var timer = actionProperties.ignoreTimeScale ?
                        Observable.Timer(TimeSpan.FromSeconds(actionProperties.invokeDelay), Scheduler.MainThreadIgnoreTimeScale) :
                        Observable.Timer(TimeSpan.FromSeconds(actionProperties.invokeDelay));

                    timer.SubscribeOnMainThread()
                        .Subscribe(_ => ExecuteAction())
                        .AddTo(this);
                }
                else
                {
                    ExecuteAction();
                }
            }
        }

        protected virtual void ExecuteAction()
        {
            if (action != null) action.Invoke();
        }

        [Serializable]
        public class Properties
        {
            public float invokeDelay = 0;
            public bool invokeOnce = false;
            public bool ignoreTimeScale = false;
            public BoolReactiveProperty invokeNOW = new BoolReactiveProperty(false);
            private bool isInvoked = false;

            public bool Invoke()
            {
                invokeNOW.Value = false;
                if (invokeOnce && isInvoked) return false;
                return isInvoked = true;
            }
        }
    }
}
